﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repasitory
{
    interface IBase_Repasitory<T> where T : class,new()
    {
        int SaveChanges();
        IEnumerable<T> AsEnumerbale();
        void Add(T model);
        void AddRange(IEnumerable<T> model);
        void Insert(int index, T model);
    }
}
