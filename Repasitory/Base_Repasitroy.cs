﻿using Repasitory.Extension;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Repasitory
{
    abstract public class Base_Repasitroy<T> : IBase_Repasitory<T> where T : class, new()
    {
        internal abstract string Path { get; }

        public Base_Repasitroy() => _list = AsEnumerbale().ToList();

        List<T> _list;

        public void Add(T model)
        {
            _list.Add(model);
        }

        public void AddRange(IEnumerable<T> model)
        {
            foreach (var item in model)
            {
                _list.Add(item);
            }
        }

        public IEnumerable<T> AsEnumerbale()
        {
            if (!File.Exists(Path))
            {
                File.Create(Path).Dispose();
            }

            T model = null;
            StreamReader reader = new StreamReader(Path, Encoding.Default);
            while (reader.EndOfStream)
            {
                string line = reader.ReadLine();
                switch (line)
                {
                    case "{":
                        model = new T();
                        break;
                    case "}":
                        yield return model;
                        break;
                    default:
                        ReadFill(line, model);
                        break;
                }
            }
            reader.Dispose();
        }

        public void Insert(int index, T model)
        {
            _list.Insert(index, model);
        }

        public int SaveChanges()
        {
            int count = 0;
            StreamWriter writer = new StreamWriter(Path, true, Encoding.Default);
            foreach (var item in _list)
            {
                writer.WriteLine("{");
                WriteModel(writer, item);
                writer.WriteLine("}");
                count++;
            }

            return count;
        }

        private void WriteModel(StreamWriter writer , T model)
        {
            var propertyinfo = model.GetType().GetProperties();
            foreach (var property in propertyinfo)
            {
                if (property.IsIgnored())
                {
                    continue;
                }

                var value = property.GetValue(model);
                if (property.PropertyType.IsEnum)
                {
                    writer.WriteLine($"{property.PropertyType}:{Convert.ToString(value)}");
                }
                else
                {
                    writer.WriteLine($"{property.PropertyType}:{value}");
                }
            }
        }

        private void OnReadFill(string[] data,T model)
        {
            string propName = data[0];
            var propertyInfo = propName.GetType().GetProperty(data[1], BindingFlags.Instance | BindingFlags.Public);
            if (propertyInfo != null)
            {
                if (propertyInfo.PropertyType.IsEnum)
                {
                    object value = Enum.Parse(propertyInfo.PropertyType, data[1]);
                    propertyInfo.SetValue(model, value);
                }
                else
                {
                    object valuess = Convert.ChangeType(data[1], propertyInfo.PropertyType);
                    propertyInfo.SetValue(model, valuess);
                }
            }
        }

        private void ReadFill(string line,T model)
        {
            string[] data = line.Split(':');
            if (data.Length < 2)
            {
                return;
            }
            OnReadFill(data, model);
        }
    }
}
