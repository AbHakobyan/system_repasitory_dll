﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Repasitory.AttributeIgnore;

namespace Repasitory.Models
{
    class Model1
    {
        public string Name { get; set; }
        public string Surename { get; set; }
        public byte Age { get; set; }

        [Ignor]
        public string FullName => $"{Name} {Surename}";

        public override string ToString() => FullName;
    }
}
