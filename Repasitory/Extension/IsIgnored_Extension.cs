﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Repasitory.AttributeIgnore;
using Repasitory.Models;

namespace Repasitory.Extension
{
    static class IsIgnored_Extension
    {
        public static bool IsIgnored(this PropertyInfo member)
        {
            return member.GetCustomAttributes() != null;
        }
    }
}
